import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','firstproject.settings')
import django
django.setup()

#Fake POP Script
import random
from first_app.models import AccessRecord, Webpage, Topic
from faker import Faker
fakegen = Faker()
topics = ['search','social','marketplace','news','games']

def add_topic():
    t = Topic.objects.get_or_create(top_name=random.choice(topics))[0]
    t.save()
    return t


def populate(N=5):
    for entry in range(N):
        top = add_topic()

#create a fake data or entry
fake_url = fakegen.url()
fake_date = fakegen.date()
fake_name = fakegen.company()

#create a new website entry
webpg = Webpage.objects.get_or_create(topic=top, url = fake_url, name=fake_name)[0]

#create a fake access record for that web Page
acc_rec = AccessRecord.objects.get_or_create(name=webpg, date=fake_date)[0]

if __name__ == '__main__':
    print("Populating Script")
    populate(20)
    print("Populating Complete")
