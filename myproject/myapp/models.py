from django.db import models

# Create your models here.
class Movie(models.Model):
    actor = models.CharField(max_length=100)
    actor_movie = models.CharField(max_length=100)
    genr = models.CharField(max_length=100)
class Songs(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    file_type = models.CharField(max_length=50)
    song_name = models.CharField(max_length=100)
