from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello world")
def hello(request):
    return render(request, 'myapp/index.html')
